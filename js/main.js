var toast = document.querySelector('.toast');
var bsToast = new bootstrap.Toast(toast);

var connectionModal = document.querySelector('#connectionModal');
var bsConnectionModal = new bootstrap.Modal(connectionModal);

window.onload = (e) => {

    var text = document.getElementById('toastText');
    if (text.innerText == '') {
        bsToast.hide();
    } else {
        bsToast.show();
    }

    var email = document.getElementById('email');
    if (email.value != '' || text.innerText == 'Vous devez être connecté pour pouvoir réserver.') {
        bsConnectionModal.show();
    } else {
        bsConnectionModal.hide();
    }

}

var inputs = document.querySelectorAll(".numberInput");
inputs.forEach((element) => {
    element.addEventListener('change', () => {
        var label = document.getElementById(element.id + 'prixTotal');
        var current = document.getElementById(element.id + 'prixCurrent');
        var inscription = document.getElementById(element.id + 'prixInscription');
        label.innerText = `Prix total: ${parseFloat(current.innerText) + (parseFloat(element.value) * parseFloat(inscription.innerText))}€`;
        console.log('changed on ' + element.id);
    });
});