-- SQLBook: Code
CREATE SCHEMA IF NOT EXISTS agence_voyage DEFAULT CHARACTER SET utf8 ;
USE agence_voyage ;

CREATE TABLE client(
   id_client INT AUTO_INCREMENT,
   nom VARCHAR(50) NOT NULL,
   prenom VARCHAR(50) NOT NULL,
   email VARCHAR(100) NOT NULL,
   password VARCHAR(60) NOT NULL,
   admin BIT(1) DEFAULT 0 NOT NULL,
   PRIMARY KEY(id_client),
   UNIQUE(email)
);

CREATE TABLE pays(
   id_pays INT AUTO_INCREMENT,
   nom VARCHAR(50),
   PRIMARY KEY(id_pays)
);

CREATE TABLE ville(
   id_ville INT AUTO_INCREMENT,
   nom VARCHAR(50),
   id_pays INT NOT NULL,
   PRIMARY KEY(id_ville),
   FOREIGN KEY(id_pays) REFERENCES pays(id_pays)
);

CREATE TABLE circuit(
   id_circuit INT AUTO_INCREMENT,
   nom_voyage VARCHAR(100) NOT NULL,
   description VARCHAR(500),
   dates_depart DATETIME NOT NULL,
   nbr_place_disponible INT NOT NULL,
   duree TIME NOT NULL,
   prix_inscription DECIMAL(15,2) NOT NULL CHECK (prix_inscription>=0),
   id_ville_depart INT NOT NULL,
   id_ville_arrive INT NOT NULL,
   PRIMARY KEY(id_circuit),
   FOREIGN KEY(id_ville_depart) REFERENCES ville(id_ville),
   FOREIGN KEY(id_ville_arrive) REFERENCES ville(id_ville)
);

CREATE TABLE reservation(
   id_reservation INT AUTO_INCREMENT,
   statut VARCHAR(50) NOT NULL DEFAULT 'En cours',
   date_reservation DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
   nb_place INT NOT NULL,
   id_circuit INT NOT NULL,
   id_client INT NOT NULL,
   PRIMARY KEY(id_reservation),
   FOREIGN KEY(id_circuit) REFERENCES circuit(id_circuit),
   FOREIGN KEY(id_client) REFERENCES client(id_client)
);

CREATE TABLE lieu_de_visite(
   id_lieu INT AUTO_INCREMENT,
   nom VARCHAR(50) NOT NULL,
   descriptif VARCHAR(500),
   image VARCHAR(500) NOT NULL DEFAULT 'default.png',
   prix_visite DECIMAL(15,2) NOT NULL CHECK (prix_visite>=0),
   id_ville INT NOT NULL,
   PRIMARY KEY(id_lieu),
   UNIQUE(nom),
   FOREIGN KEY(id_ville) REFERENCES ville(id_ville)
);

CREATE TABLE etape(
   id_etape INT AUTO_INCREMENT,
   ordre INT NOT NULL,
   dateEtape DATETIME NOT NULL,
   duree TIME NOT NULL,
   id_lieu INT NOT NULL,
   id_circuit INT NOT NULL,
   PRIMARY KEY(id_etape),
   FOREIGN KEY(id_lieu) REFERENCES lieu_de_visite(id_lieu),
   FOREIGN KEY(id_circuit) REFERENCES circuit(id_circuit)
);