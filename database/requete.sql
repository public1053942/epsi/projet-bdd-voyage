/*
    Écrire une requête permettant d’afficher pour le circuit 2, l’identifiant, descriptif, le nom
    de la ville de départ, le nom de la ville d’arrivée ainsi que le nombre d’étapes du circuit.
    Exemple de message affiché: Information sur le circuit 2 : Découverte de l’Atlas et des
    villages berbères, Casablanca, Marrakech, 4 étapes.
*/
SELECT id_circuit, description, depart.nom, arrive.nom
FROM `circuit`
JOIN ville as depart ON depart.id_ville = id_ville_depart
JOIN ville as arrive ON arrive.id_ville = id_ville_arrive
WHERE id_circuit = 2;


/*
    Écrire une requête qui supprime les lieux qui ne sont pas visités (s’il ne sont pas associés
    à une étape d’un circuit).
*/
DELETE FROM `lieu_de_visite` WHERE id_lieu NOT IN (SELECT id_lieu FROM `etape`);


/*
    Définir une requête affichant le prix d’un circuit touristique complet, c’est à dire le prix
    d’inscription du circuit auquel on additionne le prix de la visite de chaque étape du circuit.
*/
SELECT prix_inscription + SUM(prix_visite) as prixTotal
FROM `circuit`
JOIN étape ON étape.id_circuit = circuit.id_circuit
JOIN lieu_de_visite as ldv ON ldv.id_lieu = étape.id_lieu
WHERE circuit.id_circuit = 1
GROUP BY circuit.id_circuit


/*
    Ecrire une requête qui permet de rajouter une réservation d’un circuit par un client.
*/
INSERT INTO `reservation` (date_reservation, id_circuit, id_client, nb_place, statut) VALUES (NOW(),2,4,42,"En cours")


/*
    Ecrire une requête permettant de lister le nombre de réservations réalisées par un client.
    La requête doit retourner le nom et prénom des clients ainsi que le nombre de réservations.
*/
SELECT client.nom, client.prenom, COUNT(res.id_reservation) as nb_reservation
FROM `client`
JOIN `reservation` as res ON res.id_client = client.id_client
WHERE client.id_client = 1
GROUP BY client.id_client;


/*
    Listez le nom, prénom des clients n’ayant pas réalisé de réservations dans la dernière année.
*/
SELECT client.nom, client.prenom
FROM `client`
JOIN `reservation` as res ON res.id_client = client.id_client
WHERE res.date_reservation < DATE_SUB(NOW(), INTERVAL 1 YEAR);


/*
    Listez le nom des voyages qui ont encore des places disponibles. (Voyages planifiés
    dans le futur ayant un nombre de places supérieur au nombre de réservations réalisées
    (validées ou en cours). Attention, un client spécifie le nombre de places qu’il réserve.)
*/
SELECT circuit.nom_voyage
FROM `circuit`
WHERE circuit.nbr_place_disponible > (
    SELECT SUM(nb_place)
    FROM `reservation` as res
    WHERE res.id_circuit = circuit.id_circuit  AND (res.statut = "En cours" OR res.statut = "Validé")
    GROUP BY res.id_circuit
);


/*
    Affichez le nom des circuit, le nombre de jours annoncés, la date de début minimale et la
    date de fin maximale de ses étapes.
*/
SELECT circuit.nom_voyage, étape.duree, étape.dateEtape as depart_etape, ADDTIME(étape.dateEtape, étape.duree) as arrive_etape
FROM `circuit`
JOIN `étape` ON étape.id_circuit = circuit.id_circuit;