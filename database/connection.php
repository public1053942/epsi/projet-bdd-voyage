<?php

    require_once '../vendor/autoload.php';
    $dotenv = Dotenv\Dotenv::createUnsafeImmutable(substr(__DIR__, 0, -9));
    $dotenv->load();

    class DatabaseSingleton {
        private static $instance = null;
        private $conn;
        
        private function __construct()
        {
            $host= $_ENV["MYSQL_DB_HOSTNAME"];
            $port = $_ENV["MYSQL_DB_PORT"];
            $dbname = $_ENV["MYSQL_DB_DATABASE"];
            $user = $_ENV["MYSQL_DB_USERNAME"];
            $password = $_ENV["MYSQL_DB_PASSWORD"];
            try {
                $this->conn = new PDO("mysql:host=$host:$port;dbname=$dbname", $user, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));      
            } catch (PDOException $e) {
                error_log('Error whilst connecting to database. ');
                throw $e;
            }
        }
        
        public static function getInstance()
        {
            if(!self::$instance)
            {
                self::$instance = new DatabaseSingleton();
            }
            
            return self::$instance;
        }

        public function prepare(string $query): PDOStatement|bool
        {
            return $this->conn->prepare($query);
        }
        
    }

?>