<?php

    session_start();

    if (isset($_POST['logout'])) {
        session_destroy();
        header("Location: ./home.php");
        exit();
    }

    require_once("../database/connection.php");
    if (!isset($_SESSION['admin'])) {
        $_SESSION['admin'] = false;
    }

    $email = '';
    $password = '';
    $toastColor = "";
    $toastText = "";

    // Connection
    if (!isset($_SESSION['connection'])) {
        $_SESSION['connection'] = mt_rand();
    }
    $connection = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['connection_key'])) {
        if ($_POST['connection_key'] != $_SESSION['connection']) {
            $connection = false;
        } else {
            $_SESSION['connection'] = 0;
            $connection = true;
        }
    }
    if (isset($_POST['submit']) && $connection) {
            
        extract($_POST);
        $email = $_POST['email'];
        $password = $_POST['password'];

        try {
            
            $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `client` WHERE email = :email");
            $sth->bindValue('email', $email);
            $sth->execute();
            $data = $sth->fetchAll();
            if (count($data) == 1) {

                foreach ($data as $row) {

                    if (password_verify($password, $row['password'])) {
                        
                        $_SESSION['admin'] = $row['admin'];
                        $_SESSION['nom'] = $row['nom'];
                        $_SESSION['prenom'] = $row['prenom'];
                        $_SESSION['password'] = $row['password'];
                        $_SESSION['id'] = $row['id_client'];
                        
                        
                        $email = '';
                        $password = '';
                        
                        $toastColor = "success";
                        $toastText = "You are successfully connected.";

                    } else {
                        throw new PDOException("Password not match", 1);
                    }
                }

            } else {
                throw new PDOException("No data", 1);
            }

        } catch (PDOException $e) {
            $toastColor = "danger";
            $toastText = "Your email address or password is incorrect";
        }

    }
    
    // inscription
    if (!isset($_SESSION['inscription'])) {
        $_SESSION['inscription'] = mt_rand();
    }
    $inscription = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['inscription_key'])) {
        if ($_POST['inscription_key'] != $_SESSION['inscription']) {
            $inscription = false;
        } else {
            $_SESSION['inscription'] = 0;
            $inscription = true;
        }
    }
    if (isset($_POST['inscription']) && $inscription) {

        extract($_POST);
        
        try {

            $sth = DatabaseSingleton::getInstance()->prepare("SELECT email FROM `client` WHERE email=:email");
            $sth->bindValue('email', $email);
            $sth->execute();
            $data = $sth->fetchAll();

            if (count($data) == 0) {
                
                $nom = $_POST['nom'];
                $prenom = $_POST['prenom'];
                $email = $_POST['email'];
                $password = $_POST['password'];

                $sth = DatabaseSingleton::getInstance()->prepare("INSERT INTO `client` (nom, prenom, email, password) VALUES (:nom, :prenom, :email, :password)");
                $sth->bindValue('nom', $nom);
                $sth->bindValue('prenom', $prenom);
                $sth->bindValue('email', $email);
                $sth->bindValue('password', password_hash($password, PASSWORD_BCRYPT));
                $sth->execute();

                $toastColor = "success";
                $toastText = "Votre compte à bien été créé.";

            } else {
                $toastColor = "danger";
                $toastText = "Cette utilisateur existe déjà.";
            }

        } catch (PDOException $e) {
            $email = '';
            $toastColor = "danger";
            $toastText = "Une erreur est survenue. Réessayez plus tard.";
            echo($e);
        }

    }

    // delete reservation
    if (!isset($_SESSION['deleteReservation'])) {
        $_SESSION['deleteReservation'] = mt_rand();
    }
    $deleteReservation = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteReservation_key'])) {
        if ($_POST['deleteReservation_key'] != $_SESSION['deleteReservation']) {
            $deleteReservation = false;
        } else {
            $_SESSION['deleteReservation'] = 0;
            $deleteReservation = true;
        }
    }
    if (isset($_POST['deleteReservation']) && $deleteReservation) {

        extract($_POST);
        try {

            $sth = DatabaseSingleton::getInstance()->prepare("DELETE FROM `reservation` WHERE id_reservation = :id_reservation");
            $sth->bindValue('id_reservation', $_POST['id_reservation']);
            $sth->execute();
            
            $toastColor = "success";
            $toastText = "La reservation vient d'être supprimé.";

        } catch (PDOException $e) {
            $toastColor = "danger";
            $toastText = "Une erreur s'est produite lors de la suppression. Merci de réessayer plus tard.";
        }

    }

    // delete lieu
    if (!isset($_SESSION['deleteLieu'])) {
        $_SESSION['deleteLieu'] = mt_rand();
    }
    $deleteLieu = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteLieu_key'])) {
        if ($_POST['deleteLieu_key'] != $_SESSION['deleteLieu']) {
            $deleteLieu = false;
        } else {
            $_SESSION['deleteLieu'] = 0;
            $deleteLieu = true;
        }
    }
    if (isset($_POST['deleteLieu']) && $deleteLieu && $_SESSION['admin']) {
        
        extract($_POST);
        try {

            $sth = DatabaseSingleton::getInstance()->prepare("DELETE FROM `lieu_de_visite` WHERE id_lieu = :id_lieu");
            $sth->bindValue('id_lieu', $_POST['id_lieu']);
            $sth->execute();
            
            $toastColor = "success";
            $toastText = "Le lieu de visite vient d'être supprimé.";

        } catch (PDOException $e) {
            $toastColor = "danger";
            $toastText = "Une erreur s'est produite lors de la suppression. Merci de réessayer plus tard.";
        }

    }

    // delete etape
    if (!isset($_SESSION['deleteEtape'])) {
        $_SESSION['deleteEtape'] = mt_rand();
    }
    $deleteEtape = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteEtape_key'])) {
        if ($_POST['deleteEtape_key'] != $_SESSION['deleteEtape']) {
            $deleteEtape = false;
        } else {
            $_SESSION['deleteEtape'] = 0;
            $deleteEtape = true;
        }
    }
    if (isset($_POST['deleteEtape']) && $deleteEtape && $_SESSION['admin']) {

        extract($_POST);
        try {

            $sth = DatabaseSingleton::getInstance()->prepare("DELETE FROM `etape` WHERE id_etape = :id_etape");
            $sth->bindValue('id_etape', $_POST['id_etape']);
            $sth->execute();
            
            $toastColor = "success";
            $toastText = "L'étape vient d'être supprimé.";

        } catch (PDOException $e) {
            $toastColor = "danger";
            $toastText = "Une erreur s'est produite lors de la suppression. Merci de réessayer plus tard.";
        }

    }

    // delete circuit
    if (!isset($_SESSION['deleteCircuit'])) {
        $_SESSION['deleteCircuit'] = mt_rand();
    }
    $deleteCircuit = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['deleteCircuit_key'])) {
        if ($_POST['deleteCircuit_key'] != $_SESSION['deleteCircuit']) {
            $deleteCircuit = false;
        } else {
            $_SESSION['deleteCircuit'] = 0;
            $deleteCircuit = true;
        }
    }
    if (isset($_POST['deleteCircuit']) && $deleteCircuit && $_SESSION['admin']) {

        extract($_POST);
        try {

            $sth = DatabaseSingleton::getInstance()->prepare("DELETE FROM `circuit` WHERE id_circuit = :id_circuit");
            $sth->bindValue('id_circuit', $_POST['id_circuit']);
            $sth->execute();
            
            $toastColor = "success";
            $toastText = "Le circuit vient d'être supprimé.";

        } catch (PDOException $e) {
            $toastColor = "danger";
            $toastText = "Une erreur s'est produite lors de la suppression. Merci de réessayer plus tard.";
        }

    }

    // ajout lieu
    if (!isset($_SESSION['createLieu'])) {
        $_SESSION['createLieu'] = mt_rand();
    }
    $createLieu = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['createLieu_key'])) {
        if ($_POST['createLieu_key'] != $_SESSION['createLieu']) {
            $createLieu = false;
        } else {
            $_SESSION['createLieu'] = 0;
            $createLieu = true;
        }
    }
    if (isset($_POST['createLieu']) && $createLieu) {

        try {

            extract($_POST);

            $nom = $_POST['nom'];
            $description = $_POST['description'];
            $prix = $_POST['prix'];
            $ville = $_POST['ville'];

            $sth = DatabaseSingleton::getInstance()->prepare("INSERT INTO `lieu_de_visite` (nom, descriptif, prix_visite, id_ville) VALUES (:nom, :description, :prix, :id_ville)");
            $sth->bindValue('nom', $nom);
            $sth->bindValue('description', $description);
            $sth->bindValue('prix', $prix);
            $sth->bindValue('id_ville', $ville);
            $sth->execute();

            $sth = DatabaseSingleton::getInstance()->prepare("SELECT id_lieu FROM `lieu_de_visite` WHERE nom=:nom AND descriptif=:desc AND id_ville=:id_ville AND prix_visite=:prix");
            $sth->bindValue('nom', $nom);
            $sth->bindValue('desc', $description);
            $sth->bindValue('prix', $prix);
            $sth->bindValue('id_ville', $ville);
            $sth->execute();
            $data = $sth->fetch();

            if ($_FILES['file']['name'] != '') {
                $path = "../image/lieu/" . $data['id_lieu'] . ".png";
                move_uploaded_file($_FILES['file']['tmp_name'], $path);
            }

            $toastColor = "success";
            $toastText = "Le lieu de visite à bien été créé.";

        } catch (PDOException $e) {
            $toastColor = "danger";
            $toastText = "Une erreur s'est produite lors de la création du lieu de visite. Merci de réessayer plus tard.";
            echo($e);
        }

    }

    // ajout etape
    if (!isset($_SESSION['createEtape'])) {
        $_SESSION['createEtape'] = mt_rand();
    }
    $createEtape = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['createEtape_key'])) {
        if ($_POST['createEtape_key'] != $_SESSION['createEtape']) {
            $createEtape = false;
        } else {
            $_SESSION['createEtape'] = 0;
            $createEtape = true;
        }
    }
    if (isset($_POST['createEtape']) && $createEtape) {

        try {

            extract($_POST);
            
            $ordre = $_POST['ordre'];
            $date = $_POST['date'];
            $duree = $_POST['duree'];
            $lieu = $_POST['lieu'];
            $circuit = $_POST['circuit'];

            $sth = DatabaseSingleton::getInstance()->prepare("INSERT INTO `etape` (ordre, dateEtape, duree, id_lieu, id_circuit) VALUES (:ordre, :date, :duree, :lieu, :circuit)");
            $sth->bindValue('ordre', $ordre);
            $sth->bindValue('date', $date);
            $sth->bindValue('duree', $duree);
            $sth->bindValue('lieu', $lieu);
            $sth->bindValue('circuit', $circuit);
            $sth->execute();

            $toastColor = "success";
            $toastText = "L'étape à bien été créé.";

        } catch (PDOException $e) {

            $toastColor = "danger";
            $toastText = "Une erreur s'est produite lors de la création de l'étape. Merci de réessayer plus tard.";

        }

    }

    // ajouter circuit
    if (!isset($_SESSION['createCircuit'])) {
        $_SESSION['createCircuit'] = mt_rand();
    }
    $createCircuit = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['createCircuit_key'])) {
        if ($_POST['createCircuit_key'] != $_SESSION['createCircuit']) {
            $createCircuit = false;
        } else {
            $_SESSION['createCircuit'] = 0;
            $createCircuit = true;
        }
    }
    if (isset($_POST['createCircuit']) && $createCircuit) {

        extract($_POST);

        $nom = $_POST['nom_voyage'];
        $description = $_POST['description'];
        $nb_place = $_POST['nb_place'];
        $date = $_POST['date'];
        $duree = $_POST['duree'];
        $prix = $_POST['prix'];
        $depart = $_POST['depart'];
        $arrive = $_POST['arrive'];

        try {

            $sth = DatabaseSingleton::getInstance()->prepare("INSERT INTO `circuit` (nom_voyage, description, dates_depart, nbr_place_disponible, duree, prix_inscription, id_ville_depart, id_ville_arrive) VALUES (:nom, :desc, :date, :place, :duree, :prix, :depart, :arrive)");
            $sth->bindValue('nom', $nom);
            $sth->bindValue('desc', $description);
            $sth->bindValue('date', $date);
            $sth->bindValue('duree', $duree);
            $sth->bindValue('place', $nb_place);
            $sth->bindValue('prix', $prix);
            $sth->bindValue('depart', $depart);
            $sth->bindValue('arrive', $arrive);
            $sth->execute();

            $sth = DatabaseSingleton::getInstance()->prepare("SELECT id_circuit FROM `circuit` WHERE nom_voyage=:nom AND description=:desc AND duree=:duree AND dates_depart=:date AND nbr_place_disponible=:place AND prix_inscription=:prix AND id_ville_depart=:depart AND id_ville_arrive=:arrive");
            $sth->bindValue('nom', $nom);
            $sth->bindValue('desc', $description);
            $sth->bindValue('date', $date);
            $sth->bindValue('duree', $duree);
            $sth->bindValue('place', $nb_place);
            $sth->bindValue('prix', $prix);
            $sth->bindValue('depart', $depart);
            $sth->bindValue('arrive', $arrive);
            $sth->execute();
            $data = $sth->fetch();

            if ($_FILES['file']['name'] != '') {
                $path = "../image/circuit/" . $data['id_circuit'] . ".png";
                move_uploaded_file($_FILES['file']['tmp_name'], $path);
            }

            $toastColor = "success";
            $toastText = 'Le circuit à bien été créé.';

        } catch (PDOException $e) {
            $toastColor = "danger";
            $toastText = 'Une erreur est survenue. Réessayez plus tard.';
        }

    }

    // reservation
    if (!isset($_SESSION['bypass'])) {
        $_SESSION['bypass'] = mt_rand();
    }
    $reservation = false;
    if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['reservation_key'])) {
        if ($_POST['reservation_key'] != $_SESSION['bypass']) {
            $reservation = false;
        } else {
            $_SESSION['bypass'] = 0;
            $reservation = true;
        }
    }
    if (isset($_POST['reservation']) && $reservation) {

        if (!isset($_SESSION['id'])) {
                    
            $toastColor = "danger";
            $toastText = 'Vous devez être connecté pour pouvoir réserver.';

        } else {

            extract($_POST);

            try {

                $sql = "INSERT INTO `reservation` (nb_place, id_circuit, id_client) VALUES (:nb_place, :id_circuit, :id_client)";
                $sth = DatabaseSingleton::getInstance()->prepare($sql);
                $sth->bindValue('nb_place', $_POST['number']);
                $sth->bindValue('id_circuit', $_POST['id_circuit']);
                $sth->bindValue('id_client', $_SESSION['id']);
                $sth->execute();

                unset($_POST['reservation']);

                $toastColor = "success";
                $toastText = "Votre réservation c'est bien passée. Merci de votre confiance.";
                
            } catch (PDOException $e) {
                $toastColor = "danger";
                $toastText = "Une erreur s'est produite lors de votre réservation. Merci de réessayer plus tard.";
            }

        }
    
    }
    
    if (isset($_SESSION['id'])) {
        $sth = DatabaseSingleton::getInstance()->prepare("SELECT *, SUM(prix_visite) as prixTotal, depart.nom AS ville_depart, arrive.nom AS ville_arrive FROM `reservation` JOIN `circuit` ON circuit.id_circuit = reservation.id_circuit JOIN `ville` AS depart ON depart.id_ville = circuit.id_ville_depart JOIN `ville` AS arrive ON arrive.id_ville = circuit.id_ville_arrive JOIN etape ON etape.id_circuit = circuit.id_circuit JOIN lieu_de_visite as ldv ON ldv.id_lieu = etape.id_lieu WHERE id_client = :id_client");
        $sth->bindValue('id_client', $_SESSION['id']);
        $sth->execute();
        $data = $sth->fetchAll();
    } else {
        $data = null;
    }
    include '../html/index.phtml';

    ?>
        <div class="big-block">
        
    <?php
    try {
        
        $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `pays` WHERE pays.id_pays IN (SELECT ville_depart.id_pays FROM `circuit` JOIN ville AS ville_depart ON ville_depart.id_ville = circuit.id_ville_depart) OR pays.id_pays IN (SELECT ville_arrive.id_pays FROM `circuit` JOIN ville AS ville_arrive ON ville_arrive.id_ville = circuit.id_ville_arrive)");
        $sth->execute();
        $data = $sth->fetchAll();
        
        foreach ($data as $row) {
            ?>
                <div>
                    <p class="text-center"><?= $row['nom'] ?></p>
                    <a href="?pays=<?= $row['nom'] ?>"><img class="image-pays"src="../image/pays/<?= $row['id_pays'] ?>.png"></a>
                </div>
            <?php
        }

        if (isset($_GET['pays'])) {
            $sth = DatabaseSingleton::getInstance()->prepare("SELECT circuit.*, depart.nom AS ville_depart, arrive.nom AS ville_arrive FROM `circuit` JOIN `ville` AS depart ON depart.id_ville = circuit.id_ville_depart JOIN `ville` AS arrive ON arrive.id_ville = circuit.id_ville_arrive JOIN `pays` AS pays_depart ON pays_depart.id_pays = depart.id_pays JOIN `pays` AS pays_arrive ON pays_arrive.id_pays = arrive.id_pays WHERE pays_depart.nom = :pays OR pays_arrive.nom = :pays");
            $sth->bindValue('pays', $_GET['pays']);
            $msg = 'Les circuits d';
            $lettres = "aeiouyAEIOUY";
            if (str_contains($lettres, substr($_GET['pays'], 0, 1))) {
                $msg .= "'";
            } else {
                $msg .= 'e ';
            }
            $msg .= $_GET['pays'];
        } else {
            $msg = 'Ou nos différents circuits';
            $sth = DatabaseSingleton::getInstance()->prepare("SELECT circuit.*, depart.nom AS ville_depart, arrive.nom AS ville_arrive FROM `circuit` JOIN `ville` AS depart ON depart.id_ville = circuit.id_ville_depart JOIN `ville` AS arrive ON arrive.id_ville = circuit.id_ville_arrive");
        }

        $sth->execute();
        $data = $sth->fetchAll();

        if (count($data) != 0) {
            ?>
                <div class="carre-rouge margin-top text-center padding-bottom">
                    <h1 class="explorer margin-top-large white r-mt"><?= $msg ?></h1>
                    <?php
                        if (isset($_SESSION['id'])) {
                            date_default_timezone_set('Europe/Paris');
                            $currentDate = date("Y-m-d\T", strtotime('+1 day')) . "00:00";

                            if ($_SESSION['admin']) {
                            ?>
                                <button type="button" class="btn btn-danger header-button lieu-del" data-bs-toggle="modal" data-bs-target="#deleteLieuModal">
                                    Supprimer un lieu de visite
                                </button>
                                <div class="modal fade" id="deleteLieuModal" tabindex="-1" aria-labelledby="deleteLieuModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <form id="deleteLieuForm" enctype="multipart/form-data" autocomplete="off" method="post" class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="deleteLieuModalLabel">Supprimer un Lieu de visite</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <?php
                                                    $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `lieu_de_visite`");
                                                    $sth->execute();
                                                    $dataOfLieu = $sth->fetchAll();
                                                ?>
                                                <div class="mb-3">
                                                    <label for="createInputLieuDepart" class="form-label">Selectionnez le lieu à supprimer</label>
                                                    <select name="id_lieu" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                                        <?php
                                                            foreach ($dataOfLieu as $row) {
                                                                ?><option value="<?= $row['id_lieu'] ?>"><?= $row['nom'] ?></option><?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <input type="text" name="deleteLieu" value="Create Lieu" hidden>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="number" name="deleteLieu_key" value="<?= $_SESSION['deleteLieu'] ?>" hidden>
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                                <button form="deleteLieuForm" type="submit" class="btn btn-danger">Supprimer</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary header-button lieu" data-bs-toggle="modal" data-bs-target="#createLieuModal">
                                    Ajouter un lieu de visite
                                </button>
                                <div class="modal fade" id="createLieuModal" tabindex="-1" aria-labelledby="createLieuModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <form id="createLieuForm" enctype="multipart/form-data" autocomplete="off" method="post" class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="createLieuModalLabel">Nouveau Lieu de visite</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="mb-3">
                                                    <label for="createInputName" class="form-label">Nom du Lieu</label>
                                                    <input type="text" name="nom" class="form-control" aria-describedby="nom du lieu" value="" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputPassword" class="form-label">Description</label>
                                                    <textarea name="description" class="form-control" value=""></textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputPrix" class="form-label">Prix de visite</label>
                                                    <input type="number" min="0" value="1.00" step="0.01" name="prix" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" required>
                                                </div>
                                                <?php
                                                    $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `ville`");
                                                    $sth->execute();
                                                    $dataOfVille = $sth->fetchAll();
                                                ?>
                                                <div class="mb-3">
                                                    <label for="createInputVilleDepart" class="form-label">Ville du lieu</label>
                                                    <select name="ville" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                                        <?php
                                                            foreach ($dataOfVille as $row) {
                                                                ?><option value="<?= $row['id_ville'] ?>"><?= $row['nom'] ?></option><?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputFile" class="form-label">Photo du lieu</label>
                                                    <input class="form-control" name="file" accept=".png, .jpg" type="file" required>
                                                </div>
                                                <input type="text" name="createLieu" value="Create Lieu" hidden>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="number" name="createLieu_key" value="<?= $_SESSION['createLieu'] ?>" hidden>
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                                <button form="createLieuForm" type="submit" class="btn btn-success">Ajouter</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <button type="button" class="btn btn-primary header-button etape" data-bs-target="#createEtapeModal" data-bs-toggle="modal">
                                    Ajouter une étape
                                </button>
                                <div class="modal fade" id="createEtapeModal" tabindex="-1" aria-labelledby="createEtapeModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <form id="createEtapeForm" enctype="multipart/form-data" autocomplete="off" method="post" class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5">Nouvelle Etape</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="mb-3">
                                                    <label for="createInputOrdre" class="form-label">Ordre</label>
                                                    <input id="createInputOrdre" type="number" min="1" value="1" name="ordre" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputDate" class="form-label">Date de départ</label>
                                                    <input id="createInputDate" type="datetime-local" min="<?= $currentDate ?>" name="date" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputDuree" class="form-label">Durée</label>
                                                    <input type="time" min="00:00:01" name="duree" step="1" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="" required>
                                                </div>
                                                <?php
                                                    $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `lieu_de_visite`");
                                                    $sth->execute();
                                                    $dataOfLieu = $sth->fetchAll();
                                                ?>
                                                <div class="mb-3">
                                                    <label for="createInputLieuDepart" class="form-label">Lieu de visite</label>
                                                    <select name="lieu" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                                        <?php
                                                            foreach ($dataOfLieu as $ligne) {
                                                                ?><option value="<?= $ligne['id_lieu'] ?>"><?= $ligne['nom'] ?></option><?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <?php
                                                    $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `circuit`");
                                                    $sth->execute();
                                                    $dataOfCircuit = $sth->fetchAll();
                                                ?>
                                                <div class="mb-3">
                                                    <label for="createInputLieuDepart" class="form-label">Ajouter au circuit</label>
                                                    <select name="circuit" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                                        <?php
                                                            foreach ($dataOfCircuit as $ligne) {
                                                                ?><option value="<?= $ligne['id_circuit'] ?>"><?= $ligne['nom_voyage'] ?></option><?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <input type="text" name="createEtape" value="Create Etape" hidden>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="number" name="createEtape_key" value="<?= $_SESSION['createEtape'] ?>" hidden>
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                                <button form="createEtapeForm" type="submit" class="btn btn-success">Ajouter</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>

                                <button type="button" class="btn btn-primary header-button" data-bs-toggle="modal" data-bs-target="#createCircuitModal">
                                    Ajouter un circuit
                                </button>
                                <div class="modal fade" id="createCircuitModal" tabindex="-1" aria-labelledby="createCircuitModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <form id="createCircuitForm" enctype="multipart/form-data" autocomplete="off" method="post" class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="createCircuitModalLabel">Nouveau Circuit</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="mb-3">
                                                    <label for="createInputName" class="form-label">Nom du circuit</label>
                                                    <input type="text" name="nom_voyage" class="form-control" aria-describedby="nom du circuit" value="" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputPassword" class="form-label">Description</label>
                                                    <textarea name="description" class="form-control" value=""></textarea>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputNumber" class="form-label">Nombre de places</label>
                                                    <input type="number" min="0" name="nb_place" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputDate" class="form-label">Date de départ</label>
                                                    <input type="datetime-local" min="<?= $currentDate ?>" name="date" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputDuree" class="form-label">Durée</label>
                                                    <input type="time" min="00:00:01" name="duree" step="1" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="" required>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputPrix" class="form-label">Prix d'inscription</label>
                                                    <input type="number" min="0" step="0.01" name="prix" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="1.00" required>
                                                </div>
                                                <?php
                                                    $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `ville`");
                                                    $sth->execute();
                                                    $dataOfVille = $sth->fetchAll();
                                                ?>
                                                <div class="mb-3">
                                                    <label for="createInputVilleDepart" class="form-label">Ville de départ</label>
                                                    <select name="depart" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                                        <?php
                                                            foreach ($dataOfVille as $row) {
                                                                ?><option value="<?= $row['id_ville'] ?>"><?= $row['nom'] ?></option><?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputVilleArrive" class="form-label">Ville de départ</label>
                                                    <select name="arrive" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                                        <?php
                                                            foreach ($dataOfVille as $row) {
                                                                ?><option value="<?= $row['id_ville'] ?>"><?= $row['nom'] ?></option><?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="mb-3">
                                                    <label for="createInputFile" class="form-label">Photo du circuit</label>
                                                    <input class="form-control" name="file" accept=".png, .jpg" type="file" required>
                                                </div>
                                                <input type="text" name="createCircuit" value="Create Circuit" hidden>
                                            </div>
                                            <div class="modal-footer">
                                                <input type="number" name="createCircuit_key" value="<?= $_SESSION['createCircuit'] ?>" hidden>
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                                <button form="createCircuitForm" type="submit" class="btn btn-success">Ajouter</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            <?php
                            }
                        }
                        
                        $i = count($data) - 1;
                        foreach ($data as $row) {
                            $margin_bottom = $i == 0 ? ' margin-bottom' : '';
                            $modalId = str_replace(' ', '', $row['ville_depart'] . $row['ville_arrive']);
                            ?>
                                <img data-bs-toggle="modal" data-bs-target="#<?= $modalId ?>" class="text-image image-pays" src="../image/circuit/<?= $row['id_circuit'] ?>.png"><img>

                                <div class="modal fade" id="<?= $modalId ?>" tabindex="-1" aria-labelledby="<?= $modalId ?>ModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-lg">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h1 class="modal-title fs-5" id="<?= $modalId ?>ModalLabel"><?= $row['nom_voyage'] ?> (De <?= $row['ville_depart'] ?> à <?= $row['ville_arrive'] ?>)</h1>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                <?php
                                                    $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `etape` JOIN lieu_de_visite AS ldv ON ldv.id_lieu = etape.id_lieu WHERE etape.id_circuit = :id_circuit");
                                                    $sth->bindValue("id_circuit", $row["id_circuit"]);
                                                    $sth->execute();
                                                    $data = $sth->fetchAll();
                                                    $i = 0;
                                                    if (count($data) > 0) {
                                                        foreach ($data as $ligne) {
                                                            $margin_top = $i == 0 ? ' margin-top' : '';
                                                            ?>
                                                                <div class="card w-75 start-50 translate-middle-x<?= $margin_top ?> margin-bottom" style="width: 18rem;">
                                                                    <img src="../image/lieu/<?= $ligne['id_lieu'] ?>.png" class="card-img-top" alt="Image -> (<?= $ligne['nom'] ?>)">
                                                                    <div class="card-body">
                                                                        <ul class="list-group list-group-flush">
                                                                            <h5 class="card-title"><?= $ligne['ordre'] ?> : <?= $ligne['nom'] ?></h5>
                                                                            <p class="card-text"><?= $ligne['descriptif'] ?></p>
                                                                            <li class="list-group-item">Date: <?= $ligne['dateEtape'] ?></li>
                                                                            <li class="list-group-item">Durée: <?= $ligne['duree'] ?></li>
                                                                            <li class="list-group-item">Prix: <?= $ligne['prix_visite'] ?>€</li>
                                                                            <?php
                                                                                if ($_SESSION['admin']) {
                                                                                ?>
                                                                                    <li class="list-group-item">
                                                                                        <form method="post">
                                                                                            <input name="id_etape" type="number" value="<?= $ligne['id_etape'] ?>" class="btn btn-danger" hidden></input>
                                                                                            <input type="number" name="deleteEtape_key" value="<?= $_SESSION['deleteEtape'] ?>" hidden>
                                                                                            <input name="deleteEtape" type="submit" value="Supprimer l'étape" class="btn btn-danger"></input>
                                                                                        </form>
                                                                                        <form class="mt-2" method="post">
                                                                                            <a class="btn btn-primary" href="./edit.php?type=etape&id=<?= $ligne['id_etape'] ?>">Modifier l'étape</a>
                                                                                        </form>
                                                                                    </li>
                                                                                <?php
                                                                                }
                                                                            ?>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            <?php
                                                            $i++;                                                        
                                                        }
                                                        ?>
                                                            <form id="<?= $row['id_circuit'] ?>reservation" class="margin-bottom" method="post">
                                                                <label for="<?= $row['id_circuit'] ?>numberOfReservation" class="form-label">Nombre de personne(s)</label>
                                                                <input type="number" name="number" class="numberInput form-control position-relative input-number-size start-50 translate-middle-x" id="<?= $row['id_circuit'] ?>" value="1" min="1">
                                                                <input type="text" name="reservation" value="Réservation" hidden>
                                                                <input type="number" name="id_circuit" value="<?= $row['id_circuit'] ?>" hidden>
                                                                <input type="number" name="reservation_key" value="<?= $_SESSION['bypass'] ?>" hidden>
                                                            </form>
                                                            <?php
                                                                $sth = DatabaseSingleton::getInstance()->prepare("SELECT prix_inscription, prix_inscription + SUM(prix_visite) as prixTotal FROM `circuit` JOIN etape ON etape.id_circuit = circuit.id_circuit JOIN lieu_de_visite as ldv ON ldv.id_lieu = etape.id_lieu WHERE circuit.id_circuit = :id_circuit GROUP BY circuit.id_circuit");
                                                                $sth->bindValue('id_circuit', $row['id_circuit']);
                                                                $sth->execute();
                                                                $prix = $sth->fetch();
                                                            ?>
                                                            <p>Prix d'inscription: <?= $prix['prix_inscription'] ?>€</p>
                                                            <p id="<?= $row['id_circuit'] ?>prixInscription" hidden><?= $prix['prix_inscription'] ?></p>
                                                            <p id="<?= $row['id_circuit'] ?>prixTotal">Prix total: <?= $prix['prixTotal'] ?>€</p>
                                                            <p id="<?= $row['id_circuit'] ?>prixCurrent" hidden><?= $prix['prixTotal'] ?></p>
                                                        <?php
                                                    } else {
                                                        $displayReservationButton = false;
                                                        ?>
                                                            <p>Aucune étapes pour ce circuit.</p>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                                                <?php
                                                    if ($_SESSION['admin']) {
                                                        ?>
                                                            <form method="post">
                                                                <input name="id_circuit" type="number" value="<?= $row['id_circuit'] ?>" class="btn btn-danger" hidden></input>
                                                                <input type="number" name="deleteCircuit_key" value="<?= $_SESSION['deleteCircuit'] ?>" hidden>
                                                                <input name="deleteCircuit" type="submit" value="Supprimer" class="btn btn-danger"></input>
                                                            </form>
                                                            <a href="./edit.php?type=circuit&id=<?= $row['id_circuit'] ?>" class="btn btn-primary">Modifier</a>
                                                        <?php
                                                    }
                                                    if (!isset($displayReservationButton)) {
                                                        ?>
                                                            <button form="<?= $row['id_circuit'] ?>reservation" type="submit" class="btn btn-success">Réserver</button>
                                                        <?php
                                                    }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            $i-=1;
                        }
                    ?>
                </div>
            <?php
        }
        
    } catch (PDOException $e) {
        echo('Une erreur est survenue. ' . $e);
    }
    ?>        
        </div>
    <?php

?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script src="../js/main.js"></script>