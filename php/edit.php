<?php

    session_start();
    if (!isset($_SESSION['id']) || !isset($_GET['type']) || !isset($_GET['id'])) {
        header('Location: ./home.php');
        exit();
    }

    require_once('../database/connection.php');
    ?>

        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Agence Tout Risques</title>

        <link rel="stylesheet" href="../css/normalize.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
        <link href='https://fonts.googleapis.com/css?family=Montserrat' rel='stylesheet'>
        <link rel="stylesheet" type="text/css" href="../css/style.css">

        <link rel="icon" href="../image/logo.png">

    <?php

    if (isset($_POST['submit']) && $_GET['type'] == 'circuit') {

        try {
            
            extract($_POST);
            var_dump($_POST);
            
            $nom = $_POST['nom_voyage'];
            $description = $_POST['description'];
            $date = $_POST['date'];
            $duree = $_POST['duree'];
            $nb_place = $_POST['nb_place'];
            $prix = $_POST['prix'];
            $depart = $_POST['depart'];
            $arrive = $_POST['arrive'];
        
            $sth = DatabaseSingleton::getInstance()->prepare("UPDATE `circuit` SET nom_voyage=:nom, description=:desc, dates_depart=:date, nbr_place_disponible=:nb_place, duree=:duree, prix_inscription=:prix, id_ville_depart=:ville_depart, id_ville_arrive=:ville_arrive WHERE id_circuit = :id");
            $sth->bindValue('id', $_GET['id']);
            $sth->bindValue('nom', $nom);
            $sth->bindValue('desc', $description);
            $sth->bindValue('date', $date);
            $sth->bindValue('nb_place', $nb_place);
            $sth->bindValue('duree', $duree);
            $sth->bindValue('prix', $prix);
            $sth->bindValue('ville_depart', $depart);
            $sth->bindValue('ville_arrive', $arrive);
            $sth->execute();

            header("Location: ./home.php");
            exit();
        
        } catch (PDOException $e) {

            echo("Une erreur est survenue lors de la modification.");

        }

    }

    if ($_GET['type'] == 'circuit') {

        $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `circuit` WHERE id_circuit = :id");
        $sth->bindValue('id', $_GET['id']);
        $sth->execute();
        $data = $sth->fetchAll();

        foreach ($data as $row) {
            ?>
                <div class="container text-center mt-3">
                    <div class="row">
                        <div class="col">
                        </div>
                        <div class="col-6">
                        <form id="circuit" method="post">
                            <div class="mb-3">
                                <label for="createInputName" class="form-label">Nom du circuit</label>
                                <input type="text" name="nom_voyage" class="form-control" aria-describedby="nom du circuit" value="<?= $row['nom_voyage'] ?>">
                            </div>
                            <div class="mb-3">
                                <label for="createInputPassword" class="form-label">Description</label>
                                <textarea name="description" class="form-control"><?= $row['description'] ?></textarea>
                            </div>
                            <div class="mb-3">
                                <label for="createInputNumber" class="form-label">Nombre de places</label>
                                <input type="number" min="0" name="nb_place" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="<?= $row['nbr_place_disponible'] ?>" >
                            </div>
                            <div class="mb-3">
                                <label for="createInputDate" class="form-label">Date de départ</label>
                                <input type="datetime-local" min="<?= $currentDate ?>" name="date" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="<?= $row['dates_depart'] ?>">
                            </div>
                            <div class="mb-3">
                                <label for="createInputDuree" class="form-label">Durée</label>
                                <input type="time" min="00:00:01" name="duree" step="1" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="<?php echo($row['duree']) ?>">
                            </div>
                            <div class="mb-3">
                                <label for="createInputPrix" class="form-label">Prix d'inscription</label>
                                <input type="number" min="0" step="0.01" name="prix" class="form-control position-relative input-number-size start-50 translate-middle-x" aria-describedby="nom du circuit" value="<?= $row['prix_inscription'] ?>" >
                            </div>
                            <?php
                                $sth = DatabaseSingleton::getInstance()->prepare("SELECT * FROM `ville`");
                                $sth->execute();
                                $dataOfVille = $sth->fetchAll();
                            ?>
                            <div class="mb-3">
                                <label for="createInputVilleDepart" class="form-label">Ville de départ</label>
                                <select name="depart" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                    <?php
                                        foreach ($dataOfVille as $ligne) {
                                            if ($ligne['id_ville'] == $row['id_ville_depart']) {
                                                ?><option selected value="<?= $ligne['id_ville'] ?>"><?= $ligne['nom'] ?></option><?php
                                            } else {
                                                ?><option value="<?= $ligne['id_ville'] ?>"><?= $ligne['nom'] ?></option><?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="createInputVilleArrive" class="form-label">Ville de départ</label>
                                <select name="arrive" class="form-select position-relative input-number-size start-50 translate-middle-x" aria-label="Default select example">
                                    <?php
                                        foreach ($dataOfVille as $ligne) {
                                            if ($ligne['id_ville'] == $row['id_ville_arrive']) {
                                                ?><option selected value="<?= $ligne['id_ville'] ?>"><?= $ligne['nom'] ?></option><?php
                                            } else {
                                                ?><option value="<?= $ligne['id_ville'] ?>"><?= $ligne['nom'] ?></option><?php
                                            }
                                        }
                                    ?>
                                </select>
                            </div>
                            <div class="mb-3">
                                <label for="createInputFile" class="form-label">Photo du circuit</label>
                                <input class="form-control" name="file" accept=".png, .jpg" type="file">
                            </div>
                            <button form="circuit" name="submit" type="submit" class="btn btn-primary">Modifier</button>
                        </form>
                        </div>
                        <div class="col">
                        </div>
                    </div>
                </div>
            <?php
        }

    }

?>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.8/dist/umd/popper.min.js" integrity="sha384-I7E8VVD/ismYTF4hNIPjVp/Zjvgyol6VFvRkX/vR+Vc4jQkC+hVqc2pM8ODewa9r" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.min.js" integrity="sha384-BBtl+eGJRgqQAUMxJ7pMwbEyER4l1g+O15P+16Ep7Q9Q+zqX6gSbd85u4mG4QzX+" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>